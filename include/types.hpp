#include <cstring>
#include <string>
#include <vector>
#include <map>

// #include <opencv2/core/core.hpp>
// #include <opencv2/highgui/highgui.hpp>
// #include <opencv2/imgproc/imgproc.hpp>

typedef struct Buffer {

  void*  buffer_data; /**< buffer pointer */
  size_t buffer_size; /**< size of the buffer in bytes */

  /* Default Constructor */
  Buffer() {
    buffer_data = NULL;
    buffer_size = 0;
  }

  /* Parameterized Constructor */
  Buffer(void*  buffer_data_, size_t buffer_size_) {
    buffer_data = buffer_data_;
    buffer_size = buffer_size_;
  }

} Buffer;

typedef struct Box {
  unsigned char *Buffer;
  float x1;
  float y1; /* (x1,y1) - top left corner of the box */
  float x2;
  float y2; /* (x2,y2) - bottom right corner of the box */

  /**
   * Defafult constructor
   */
  Box() {
    x1 = -1;
    y1 = -1;
    x2 = -1;
    y2 = -1;
  }

  /**
   * Initialization constructor
   */
  Box(float x1_, float y1_, float x2_, float y2_) {
    x1 = x1_;
    y1 = y1_;
    x2 = x2_;
    y2 = y2_;
  }

  Box(unsigned char *Buffer_ip, float x1_, float y1_, float x2_, float y2_) {
    Buffer = Buffer_ip;
    x1 = x1_;
    y1 = y1_;
    x2 = x2_;
    y2 = y2_;
  }

}Box;

typedef struct Face {

  Box box;           /* Detected face coordinates (x1,y1) & (x2, y2) */
  float probability; /* Probability of detected face */
  /**
   * Default constructor
   */
  Face() {
    box      = Box();
    probability = 0.0f;
  }

  /**
   * Initialization constructor
   */
  Face(Box box_, float probability_) {
    box       = box_;
    probability = probability_;
  }

  bool operator <(const Face &s2) const {
      return this->probability < s2.probability;
  }
  bool operator >(const Face &s2) const {
      return this->probability > s2.probability;
  }

}Face;