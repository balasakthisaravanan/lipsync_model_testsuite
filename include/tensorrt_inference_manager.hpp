#ifndef __TENSORRT_INFERENCE_MANAGER_H__
#define __TENSORRT_INFERENCE_MANAGER_H__

#include <assert.h>
#include <time.h>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>

#include "NvInfer.h"
#include "NvInferPlugin.h"
#include <cuda_runtime_api.h>

#define NV_CUDA_CHECK1(status)                       \
    {                                                \
        if (status != 0)                             \
        {                                            \
            std::cout << "Cuda failure: " << status; \
            abort();                                 \
        }                                            \
    }

class TRTLogger1 : public nvinfer1::ILogger
{
    void log(Severity severity, const char *msg) override
    {
        // suppress info-level messages
        if (severity != Severity::kINFO)
            std::cout << msg << std::endl;
    }
};

class TensorRTInferenceManager
{
public:
    int netBatchSize, iHeight, iWidth, iChannels;
    std::vector<std::vector<int>> oDims;
    std::string suffix;
    int inputIndex;
    std::vector<int> outputIndex;

    nvinfer1::ICudaEngine *engine;
    nvinfer1::IExecutionContext *context;
    nvinfer1::Dims idimensions;
    std::vector<long int> output_buffer_sizes;
    void **buffer;
    cudaStream_t stream;

    TensorRTInferenceManager(std::string engine_path, int batch_size);
    void runInference(float *input_buffer, float *output_buffer, int batch_size);
    void runInference(float *input_buffer, float *output_buffer);
    void runInference(unsigned char *input_buffer, float *output_buffer);

    ~TensorRTInferenceManager();
};

#endif