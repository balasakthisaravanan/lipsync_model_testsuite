#ifndef __LIP_UTILS_HPP__
#define __LIP_UTILS_HPP__

#include <iostream>
#include <vector>
#include <ostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#ifdef TRT
#include "tensorrt_inference_manager.hpp"
#elif OPENVINO
#include "openvino_inference_manager.hpp"
#endif

using namespace std;
using namespace cv;

float* preProcessLipDetect(uint8_t *image, int IC, int IH, int IW, int c, int h, int w);

#ifdef OPENVINO
float** postProcessLipDetect(OpenVINOInferenceManager *LipDetectNet, float* output_buffer, int c, int h, int w, int &box_count);
#elif TRT
float** postProcessLipDetect(TensorRTInferenceManager *LipDetectNet, float* output_buffer, int c, int h, int w, int &box_count);
#endif

void convertFlowToImage(const Mat &flow_x, const Mat &flow_y, Mat &img_x, Mat &img_y, double lowerBound, double higherBound);

#endif