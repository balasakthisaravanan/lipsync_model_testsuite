#ifndef __FACE_DETECT_HPP__
#define __FACE_DETECT_HPP__

#include <iostream>
#include <stdlib.h>
#ifdef TRT
#include "tensorrt_inference_manager.hpp"
#elif OPENVINO
#include "openvino_inference_manager.hpp"
#endif
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

#define SIZE 416

class FaceDetect{    
    private:
#ifdef TRT
    TensorRTInferenceManager* FaceNet;
#elif OPENVINO
    OpenVINOInferenceManager* FaceNet;
#endif
    float* input_buffer;
    float* output_buffer;
    int orig_w, orig_h, orig_c;

    public:
    FaceDetect(string face_model, int batch_size);
    int pre_process(Mat frame);
    int execute();
    int post_process();
    float* get_output();
};

#endif