#ifndef __OPENVINO_INFERENCE_MANAGER_H__
#define __OPENVINO_INFERENCE_MANAGER_H__

#include <inference_engine.hpp>

#define DEBUG_INFER_STATUS(...);
class OpenVINOInferenceManager {
    public:
        int netBatchSize, iHeight, iWidth, iChannels;

        InferenceEngine::InputsDataMap inputInfo;
        InferenceEngine::OutputsDataMap outputInfo;

        std::vector<long int> output_buffer_sizes; 
        std::vector<std::string> output_blob_name;

        OpenVINOInferenceManager(std::string xml_path, int batch_size);

        void runInference(float *input_buffer,
                          float *output_buffer);
        
        void runInference(unsigned char *input_buffer,
                          float *output_buffer);

        InferenceEngine::CNNNetwork network;
        InferenceEngine::InferRequest::Ptr infPtr;

        ~OpenVINOInferenceManager();

    private:
        std::string inputName;
        InferenceEngine::Core ie;
};

#endif //__OPENVINO_INFERENCE_MANAGER_H__
