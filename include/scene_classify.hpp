#ifndef  __SCENE_CLASSIFY_HPP__
#define  __SCENE_CLASSIFY_HPP__

#include <iostream>
#include <stdlib.h>
#ifdef TRT
#include "tensorrt_inference_manager.hpp"
#elif OPENVINO
#include "openvino_inference_manager.hpp"
#endif
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

#define SIZE 224

class SceneClassify{    
    private:
#ifdef TRT
    TensorRTInferenceManager* SceneNet;
#elif OPENVINO
    OpenVINOInferenceManager* SceneNet;
#endif
    float* input_buffer;
    float* output_buffer;
    bool valid;

    public:
    SceneClassify(string scene_model, int batch_size);
    int pre_process(Mat frame);
    int execute();
    int post_process();
    pair<bool, float> get_output();
};

#endif