#include "types.hpp"
#include <algorithm>
#include <assert.h>

#include <string>

void yoloPreprocessImage(unsigned char *image_data, float *resized_image, int image_height, int image_width, int resized_height, int resized_width, int channels);
std::vector<Face> nonMaximumSuppression(const float nmsThresh, std::vector<Face> binfo);

// #if GPU_DEVICE
void postprocessDetections(float *detection_net_output, int image_width, int image_height,
	                        int detection_width, int detection_height, float NMSThresh);
// #else
void parseYoloOutputForBoxOutputs(float *network_out, int resized_im_h, int resized_im_w, int height, int width, std::vector<long int> output_buffer_sizes, float conf_thresh=0.5, float iou_thresh=0.5);
// #endif //