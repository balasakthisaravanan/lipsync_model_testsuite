#ifndef __LIP_MVT_HPP__
#define __LIP_MVT_HPP__

#include <iostream>
#include <stdlib.h>
#ifdef TRT
#include "tensorrt_inference_manager.hpp"
#elif OPENVINO
#include "openvino_inference_manager.hpp"
#endif
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

#define SIZE 48

class LipMvt{    
    private:
#ifdef TRT
    TensorRTInferenceManager* LipMvtNet;
#elif OPENVINO
    OpenVINOInferenceManager* LipMvtNet;
#endif
    unsigned char* input_buffer;
    float* output_buffer;
    int orig_w, orig_h, orig_c;

    public:
    LipMvt(string lip_mvt_model, int batch_size);
    int pre_process(vector<Mat> lips, int bound);
    int execute();
    int post_process();
    float get_output();
};

#endif