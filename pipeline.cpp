#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <iomanip>
#include <string>

#include "scene_classify.hpp"
#include "face_detect.hpp"
#include "lip_detect.hpp"
#include "lip_mvt.hpp"

using namespace std;
using namespace cv;

struct Config
{
	string video_path;
	bool dump_crop;
	string dump_path;
	FILE* scene_dump;
	FILE* face_dump;
	FILE* lip_dump;
	FILE* lip_mvt_dump;
	string scene_model;
	string face_model;
	string lip_model;
	string lip_mvt_model;

	~Config(){
		fclose(scene_dump);
		fclose(face_dump);
		fclose(lip_dump);
		fclose(lip_mvt_dump);
	}
} config;

void parse_config_file(string config_file_path);

void dump_output(FILE *fp, int frame_no, const char* output);

void dump_output(FILE *fp, int frame_no, int x1, int y1, int x2, int y2, float conf);

int main(int argc, char** argv)
{

	string config_file_path = string(argv[1]);

	parse_config_file(config_file_path);

	cout << config.video_path << endl
		 << config.dump_path << endl
		 << config.scene_model << endl
		 << config.face_model << endl
		 << config.lip_model << endl
		 << config.lip_mvt_model << endl;

	SceneClassify scene_obj = SceneClassify(config.scene_model, 1);
	FaceDetect face_obj = FaceDetect(config.face_model, 1);
	LipDetect lip_obj = LipDetect(config.lip_model, 1);
	LipMvt lip_mvt_obj = LipMvt(config.lip_mvt_model, 1);

	VideoCapture cap;

	cap.open(config.video_path);

	Mat frame;

	vector<Mat> lip_vec;

	if (!cap.isOpened())
	{
		cerr << "ERROR! Unable to open camera\n";
		return -1;
	}

	for (int frame_itr=1;;frame_itr++)
	{
		cap.read(frame);
		if (frame.empty())
		{
			cout << "Done" << endl;
			break;
		}

		scene_obj.pre_process(frame);
		scene_obj.execute();
		scene_obj.post_process();

		dump_output(config.scene_dump, frame_itr, to_string(scene_obj.get_output().second).c_str());

		if (!scene_obj.get_output().first)
		{
			dump_output(config.scene_dump, frame_itr, "InValid Scene");
			continue;
		}

		face_obj.pre_process(frame);
		face_obj.execute();
		face_obj.post_process();

		for (int face_itr = 0; face_itr < face_obj.get_output()[0]; face_itr++)
		{
			int x1 = (int)face_obj.get_output()[face_itr * 5 + 1];
			int y1 = (int)face_obj.get_output()[face_itr * 5 + 2];
			int x2 = (int)face_obj.get_output()[face_itr * 5 + 3];
			int y2 = (int)face_obj.get_output()[face_itr * 5 + 4];
			float conf = face_obj.get_output()[face_itr * 5 + 5];

			dump_output(config.face_dump, frame_itr, x1, y1, x2, y2, conf);

			if (x1 <= 0 || y1 <= 0 || x2 - x1 >= frame.cols || y2 - y1 >= frame.rows)
				continue;

			Rect face_box(x1, y1, x2 - x1, y2 - y1);
			Mat face_crop = frame(face_box);

			if (config.dump_crop)
				imwrite(config.dump_path + to_string(frame_itr) + "_face_" + to_string(face_itr) + ".jpg", face_crop);

			lip_obj.pre_process(face_crop);
			lip_obj.execute();
			lip_obj.post_process();

			x1 = lip_obj.get_output()[0];
			y1 = lip_obj.get_output()[1];
			x2 = lip_obj.get_output()[2];
			y2 = lip_obj.get_output()[3];
			conf = lip_obj.get_output()[4];

			dump_output(config.lip_dump, frame_itr, x1, y1, x2, y2, conf);

			if (x1 <= 0 || y1 <= 0 || x2 - x1 >= face_crop.cols || y2 - y1 >= face_crop.rows)
				continue;

			Rect lip_box(x1, y1, x2 - x1, y2 - y1);
			Mat lip_crop = face_crop(lip_box);

			if (config.dump_crop)
				imwrite(config.dump_path + to_string(frame_itr) + "_lip_" + to_string(face_itr) + ".jpg", lip_crop);

			if(lip_vec.size() == 6){
				lip_mvt_obj.pre_process(lip_vec, 20);
				lip_mvt_obj.execute();
				dump_output(config.lip_mvt_dump, frame_itr, to_string(lip_mvt_obj.get_output()>=0.7f ? 1 : 0).c_str());
				lip_vec.erase(lip_vec.begin());
				lip_vec.push_back(lip_crop);
			}else{
				lip_vec.push_back(lip_crop);
			}
		}
	}

	return 0;
}

void parse_config_file(string config_file_path)
{
	ifstream fp(config_file_path);
	string s;
	while (true)
	{
		getline(fp, s, ':');
		if (s == "video_path")
		{
			getline(fp, s, '\n');
			config.video_path = s.substr(1);
		}
		else if (s == "dump_crop")
		{
			getline(fp, s, '\n');
			istringstream(s.substr(1)) >> boolalpha >> config.dump_crop;
		}
		else if (s == "dump_path")
		{
			getline(fp, s, '\n');
			config.dump_path = s.substr(1);
		}
		else if (s == "scene_dump")
		{
			getline(fp, s, '\n');
			config.scene_dump = fopen(s.substr(1).c_str(), "w");
		}
		else if (s == "face_dump")
		{
			getline(fp, s, '\n');
			config.face_dump = fopen(s.substr(1).c_str(), "w");
		}
		else if (s == "lip_dump")
		{
			getline(fp, s, '\n');
			config.lip_dump = fopen(s.substr(1).c_str(), "w");
		}
		else if (s == "lip_mvt_dump")
		{
			getline(fp, s, '\n');
			config.lip_mvt_dump = fopen(s.substr(1).c_str(), "w");
		}
		else if (s == "scene_model")
		{
			getline(fp, s, '\n');
			config.scene_model = s.substr(1);
		}
		else if (s == "face_model")
		{
			getline(fp, s, '\n');
			config.face_model = s.substr(1);
		}
		else if (s == "lip_model")
		{
			getline(fp, s, '\n');
			config.lip_model = s.substr(1);
		}
		else if (s == "lip_mvt_model")
		{
			getline(fp, s, '\n');
			config.lip_mvt_model = s.substr(1);
		}
		else
			break;
	}
}

void dump_output(FILE *fp, int frame_no, const char* output){
	fprintf(fp, "%d, %s\n", frame_no, output);
}

void dump_output(FILE *fp, int frame_no, int x1, int y1, int x2, int y2, float conf){
	fprintf(fp, "%d, %d, %d, %d, %d, %f\n", frame_no, x1, y1, x2, y2, conf);
}