#include "openvino_inference_manager.hpp"

#ifdef LS_DEBUG
#define ERR_PRINT(s) std::cerr<<"Error - "<<s<<" "<<__FILE__<<" : "<<__LINE__<<std::endl;
#else
#define ERR_PRINT(s)
#endif

#if LS_DEBUG
#define DEBUG_INFO(...) {printf(__VA_ARGS__); fflush(stdout);}
#else
#define DEBUG_INFO(...) ;
#endif

OpenVINOInferenceManager::OpenVINOInferenceManager(std::string xml_path, int batch_size) {
    try {

        network = ie.ReadNetwork(xml_path);

        DEBUG_INFO("[OpenVINOInferenceManager] Loading model: %s\n", xml_path.c_str());

        // obtain info about input and output blobs
        inputInfo  = network.getInputsInfo();
        outputInfo = network.getOutputsInfo();

		if(xml_path.find("Model1") == std::string::npos){
			iHeight   = inputInfo.begin()->second->getTensorDesc().getDims()[2];
			iWidth    = inputInfo.begin()->second->getTensorDesc().getDims()[3];
			iChannels = inputInfo.begin()->second->getTensorDesc().getDims()[1];
		}else{
			iHeight   = inputInfo.begin()->second->getTensorDesc().getDims()[1];
			iWidth    = inputInfo.begin()->second->getTensorDesc().getDims()[2];
			iChannels = inputInfo.begin()->second->getTensorDesc().getDims()[3];
		}

        // set batch size
        netBatchSize = batch_size;
        DEBUG_INFO("[OpenVINOInferenceManager] Setting batch size: %d \n", netBatchSize);
        network.setBatchSize(netBatchSize);

        // input name is usable
        inputName = inputInfo.begin()->first;
        InferenceEngine::InputInfo::Ptr& input = inputInfo.begin()->second;

        // set default input precision as FP32
        input->setPrecision(InferenceEngine::Precision::FP32);

        // set NCHW as the default input layout
        input->getInputData()->setLayout(InferenceEngine::Layout::NCHW);

        for (auto &output : outputInfo) {

            // output blob names are usable
            output_blob_name.push_back(output.first);

            // set FP32 as default output precision
            output.second->setPrecision(InferenceEngine::Precision::FP32);

            // compute total size of the output buffer
            long int out_size = 1;
            for(int i = 0; i < output.second->getTensorDesc().getDims().size(); i++)
                out_size *= output.second->getTensorDesc().getDims()[i];
            output_buffer_sizes.push_back(out_size);
        }

        /* UNCOMMENT for single threaded build */
        /*
        ie.SetConfig({{ CONFIG_KEY(CPU_THREADS_NUM), std::to_string(1) },
                    { CONFIG_KEY(CPU_BIND_THREAD), CONFIG_VALUE(YES) }, 
                    { CONFIG_KEY(PERF_COUNT), CONFIG_VALUE(NO) }}, "CPU");
        */

        std::map<std::string, std::string> configDict = {};

        // default device type is an Intel CPU
        InferenceEngine::ExecutableNetwork execNetwork = ie.LoadNetwork(network, "CPU", configDict);

        // can run inference with this ptr
        infPtr = execNetwork.CreateInferRequestPtr();

        DEBUG_INFO("[OpenVINOInferenceManager] Model Successfully Loaded %s\n", xml_path.c_str());

    }
	catch (const std::exception & ex) {
		std::cout << ex.what() << std::endl;
	}
}

void OpenVINOInferenceManager::runInference(float *input_buffer, float *output_buffer) {
	try {
		InferenceEngine::Blob::Ptr frameBlob    = infPtr->GetBlob(inputName);
        InferenceEngine::MemoryBlob::Ptr mimage = InferenceEngine::as<InferenceEngine::MemoryBlob>(frameBlob);

        // get OpenVINO blob pointer
        auto minputHolder = mimage->wmap();
        float *blob_data  = minputHolder.as<float *>();

        // copy input blob and infer
        memcpy(blob_data, input_buffer, sizeof(float)*netBatchSize*iChannels*iHeight*iWidth);
		infPtr->Infer();

        // iterate and copy all outputs to buffer
		for (int i = 0; i < output_blob_name.size(); i++) {

			InferenceEngine::Blob::Ptr blob = infPtr->GetBlob(output_blob_name[i]);

            // copy OpenVINO output blobs to buffer
            memcpy(output_buffer,
                   blob->buffer().as<InferenceEngine::PrecisionTrait<InferenceEngine::Precision::FP32>::value_type *>(),
                   sizeof(float) * output_buffer_sizes[i]);

            output_buffer += output_buffer_sizes[i];
		}
	}
	catch (const std::exception & ex) {
		ERR_PRINT(ex.what());
	}
}

void OpenVINOInferenceManager::runInference(unsigned char *input_buffer, float *output_buffer) {
	try {
		InferenceEngine::Blob::Ptr frameBlob    = infPtr->GetBlob(inputName);
        InferenceEngine::MemoryBlob::Ptr mimage = InferenceEngine::as<InferenceEngine::MemoryBlob>(frameBlob);

        // get OpenVINO blob pointer
        auto minputHolder = mimage->wmap();
        float *blob_data  = minputHolder.as<float *>();

        // copy input blob and infer
        for (size_t pid = 0; pid < netBatchSize*iChannels*iHeight*iWidth; ++pid) {
            blob_data[pid] = (float) input_buffer[pid];
        }

        infPtr->Infer();

        // iterate and copy all outputs to buffer
		for (int i = 0; i < output_blob_name.size(); i++) {

			InferenceEngine::Blob::Ptr blob = infPtr->GetBlob(output_blob_name[i]);

            // copy OpenVINO output blobs to buffer
            memcpy(output_buffer,
                   blob->buffer().as<InferenceEngine::PrecisionTrait<InferenceEngine::Precision::FP32>::value_type *>(),
                   sizeof(float) * output_buffer_sizes[i]);

            output_buffer += output_buffer_sizes[i];
		}
	}
	catch (const std::exception & ex) {
		ERR_PRINT(ex.what());
	}
}

OpenVINOInferenceManager::~OpenVINOInferenceManager() {

}