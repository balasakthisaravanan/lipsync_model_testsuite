#include "lip_utils.hpp"

#ifdef OPENVINO
using namespace InferenceEngine;
#endif

struct DetectionObject {
    int xmin, ymin, xmax, ymax, class_id;
    float confidence;

    DetectionObject(double x, double y, double h, double w, int class_id, float confidence, float h_scale, float w_scale, int original_im_h, int original_im_w) {

        this->xmin = static_cast<int>((x - w / 2) * w_scale);
        this->ymin = static_cast<int>((y - h / 2) * h_scale);
        this->xmax = static_cast<int>(this->xmin + w * w_scale);
        this->ymax = static_cast<int>(this->ymin + h * h_scale);
        this->class_id = class_id;
        this->confidence = confidence;

        isItwithinImageBoundaries(&this->xmin, &this->ymin, &this->xmax, &this->ymax, original_im_h, original_im_w);
    }

    bool operator<(const DetectionObject &s2) const {
        return this->confidence > s2.confidence;
    }

    void isItwithinImageBoundaries(int *xmin, int *ymin, int *xmax, int *ymax, int original_im_h, int original_im_w) {
        if(*(xmin) < 0) *(xmin) = 0;
        if(*(xmin) > original_im_w) *(xmin) = original_im_w;
        if(*(xmax) < 0) *(xmax) = 0;
        if(*(xmax) > original_im_w) *(xmax) = original_im_w;
        if(*(ymin) < 0) *(ymin) = 0;
        if(*(ymin) > original_im_h) *(ymin) = original_im_h;
        if(*(ymax) < 0) *(ymax) = 0;
        if(*(ymax) > original_im_h) *(ymax) = original_im_h;
    }
    friend ostream& operator<<(ostream& out, const DetectionObject& obj) {

        out << "[" << obj.xmin << ", " << obj.ymin << "] ["
            << obj.xmax << ", " << obj.ymax << "] id: " << obj.class_id << " confi: " << obj.confidence;
        return out;
    }
};

void normalize_face_input(unsigned char *image_data, 
                        float *f_image, int channels, int height,
                        int width) {

  int convert_bgr_rgb=0, split_channels=1, normalize=1,num=1;
  float mean_value[3]={0.000000,0.000000,0.000000};
  float std_value[3]={1.000000,1.000000,1.000000};

  int max_value = normalize ? 255 : 1;
  int image_index, f_image_index;
  for (int n = 0; n < num; n++)
    for (int c = 0; c < channels; c++)
      for (int h = 0; h < height; h++) {
        for (int w = 0; w < width; w++) {

          image_index = n * height * width * channels +
                            h * width * channels + w * channels +
                            (convert_bgr_rgb ? channels - c - 1 : c);
          f_image_index = split_channels ? n * channels * height * width +
                              c * height * width + h * width + w : image_index;

          float n_value =
              (((unsigned char *)image_data)[image_index]) / (float)max_value;
          f_image[f_image_index] = (n_value - mean_value[c]) / std_value[c];
        }
     }
}

void ResizeDetectImage(float *input, float *output, int o_w, int o_h, int r_w,
	int r_h, int channels) {
	float *part = (float *)malloc(r_w * o_h * channels * sizeof(float));
	// image part = make_image(w, o_h, channels);
	int r, c, k;
	float w_scale = (float)(o_w - 1) / (r_w - 1);
	float h_scale = (float)(o_h - 1) / (r_h - 1);

	int oh_ow = o_h * o_w;
	int oh_rw = o_h * r_w;
	int rh_rw = r_h * r_w;

	for (k = 0; k < channels; ++k) {
		#pragma omp parallel for private(r, c)
		for (r = 0; r < o_h; ++r) {
			for (c = 0; c < r_w - 1; ++c) {
				float sx = c * w_scale;
				int ix = (int)sx;
				float dx = sx - ix;
				float val = (1 - dx) * input[k * oh_ow +
						r * o_w +
						ix] +  // get_pixel(im, ix, r, k) +
						dx * input[k * oh_ow +
						r * o_w +
						(ix + 1)];  // get_pixel(im, ix + 1, r, k);
				part[k * oh_rw +
					r * r_w + c] =
					val;  // set_pixel(part, c, r, k, val);
			}
     		part[k * oh_rw +
	  			r * r_w + c] = input[k * oh_ow +
					r * o_w +
					o_w - 1];  // get_pixel(im, o_w - 1, r, k);
		}
	}
	for (k = 0; k < channels; ++k) {
        #pragma omp parallel for private(r, c)
		for (r = 0; r < r_h; ++r) {
			float sy = r * h_scale;
			int iy = (int)sy;
			float dy = sy - iy;
			for (c = 0; c < r_w; ++c) {
				float val =
					(1 - dy) *
					part[k * oh_rw +
					iy * r_w +
					c];  // get_pixel(part, c, iy, k);
				output[k * rh_rw +
					r * r_w +
					c] =
					val;  // set_pixel(resized, c, r, k, val);
			}
			if (r == r_h - 1) continue;
			for (c = 0; c < r_w; ++c) {
				float val = dy * part[k * oh_rw + (iy + 1) * r_w +
					c];  // get_pixel(part, c, iy + 1, k);
				output[k * rh_rw + r * r_w + c] +=
					val;  // add_pixel(resized, c, r, k, val);

			}
		}
	}

	free(part);
}

float* preProcessLipDetect(uint8_t *image, int IC, int IH, int IW, int c, int h, int w) {

  float *f_image, *resized_image;

  f_image = (float *)malloc(c * w * h * sizeof(float));

  normalize_face_input(image, f_image, c, h, w);

  resized_image = (float *)malloc(IH * IW * IC * sizeof(float));

  ResizeDetectImage(f_image, resized_image, w, h, IW, IH, IC);

  // we don't need f_image anymore
  free(f_image);

  return resized_image;
}

int EntryIndex(int side, int lcoords, int lclasses, int location, int entry) {
    int n = location / (side * side);
    int loc = location % (side * side);
    return n * side * side * (lcoords + lclasses + 1) + entry * side * side + loc;
}

double IntersectionOverUnion(const DetectionObject &box_1,
                             const DetectionObject &box_2) {
    double width_of_overlap_area = fmin(box_1.xmax, box_2.xmax) - fmax(box_1.xmin, box_2.xmin);
    double height_of_overlap_area = fmin(box_1.ymax, box_2.ymax) - fmax(box_1.ymin, box_2.ymin);
    double area_of_overlap;
    if (width_of_overlap_area < 0 || height_of_overlap_area < 0)
        area_of_overlap = 0;
    else
        area_of_overlap = width_of_overlap_area * height_of_overlap_area;
    double box_1_area = (box_1.ymax - box_1.ymin)  * (box_1.xmax - box_1.xmin);
    double box_2_area = (box_2.ymax - box_2.ymin)  * (box_2.xmax - box_2.xmin);
    double area_of_union = box_1_area + box_2_area - area_of_overlap;
    return area_of_overlap / area_of_union;
}
void convertFlowToImage(const Mat &flow_x, const Mat &flow_y, Mat &img_x, Mat &img_y, double lowerBound, double higherBound) {
        #define CAST(v, L, H) ((v) > (H) ? 255 : (v) < (L) ? 0 : cvRound(255*((v) - (L))/((H)-(L))))
    unsigned int size = flow_x.rows * flow_y.cols;

        #pragma omp parallel for
        for (int iter = 0;
             iter < size;
             iter ++) {
          int i = iter / flow_y.cols;
          int j = iter % flow_y.cols;
          float x = flow_x.at<float>(i,j);
          float y = flow_y.at<float>(i,j);
          img_x.at<uchar>(i,j) = CAST(x, lowerBound, higherBound);
          img_y.at<uchar>(i,j) = CAST(y, lowerBound, higherBound);
        }
#undef CAST
}

#ifdef OPENVINO
void ParseYOLOV2Output(const Blob::Ptr &blob,
                       const CNNLayerPtr &layer,
                       const unsigned long resized_im_h,
                       const unsigned long resized_im_w,
                       const unsigned long original_im_h,
                       const unsigned long original_im_w,
                       const unsigned int h,
                       const unsigned int w,
                       const unsigned int out_blob_h,
                       const unsigned int out_blob_w,
                       const double threshold,
                       std::vector<DetectionObject> &objects, std::vector<float> anchors) 
{
    // --------------------------- Validating output parameters -------------------------------------
    if (layer->type != "RegionYolo")
        throw std::runtime_error("Invalid output type: " + layer->type + ". RegionYolo expected");
    // --------------------------- Extracting layer parameters -------------------------------------
    const int num = layer->GetParamAsInt("num");
    const int coords = layer->GetParamAsInt("coords");
    const int classes = layer->GetParamAsInt("classes");


    
    try {
        anchors = layer->GetParamAsFloats("anchors");
    }
    catch (...) {}
    auto side = out_blob_w;
    auto side_square = side * side;
    const float *output_blob = blob->buffer().as<PrecisionTrait<Precision::FP32>::value_type *>();

    // --------------------------- Parsing YOLO Region output -------------------------------------
    for (int i = 0; i < side_square; ++i) {
        int row = i / side;
        int col = i % side;
        for (int n = 0; n < num; ++n) {
            int obj_index = EntryIndex(side, coords, classes, n * side * side + i, coords);
            int box_index = EntryIndex(side, coords, classes, n * side * side + i, 0);
            double scale = output_blob[obj_index];
            if (scale < threshold)
                continue;

            double x = (col + output_blob[box_index + 0 * side_square]) / side * original_im_w;

            double y = (row + output_blob[box_index + 1 * side_square]) / side * original_im_h;
            double height = std::exp(output_blob[box_index + 3 * side_square]) * anchors[2 * n + 1] / side * original_im_h;
            double width  = std::exp(output_blob[box_index + 2 * side_square]) * anchors[2 * n] / side * original_im_w;
            for (int j = 0; j < classes; ++j) {
                int class_index = EntryIndex(side, coords, classes, n * side_square + i, coords + 1 + j);
                float prob = scale * output_blob[class_index];
                if (prob < threshold)
                    continue;

                DetectionObject obj(x, y, height, width, j, prob,
                                    static_cast<float>(h) / static_cast<float>(resized_im_h),
                                    static_cast<float>(w) / static_cast<float>(resized_im_w),
                                    h, w);
                objects.push_back(obj);
            }
        }
    }
}

float** postProcessLipDetect(OpenVINOInferenceManager *LipDetectNet, float* output_buffer, int c, int h, int w, int &box_count) {

    std::vector<DetectionObject> objects;
    std::vector<float> anchors =  {1.08,1.19,  3.42,4.41,  6.63,11.38,  9.42,5.11,  16.62,10.52};
    int IC, IH, IW;

    IC = LipDetectNet->iChannels; IH = LipDetectNet->iHeight; IW = LipDetectNet->iWidth;

    for (auto &output : LipDetectNet->outputInfo) {
        auto output_name = output.first;
        InferenceEngine::CNNLayerPtr layer = LipDetectNet->network.getLayerByName(output_name.c_str());
        InferenceEngine::Blob::Ptr blob = LipDetectNet->infPtr->GetBlob(output_name);

        int OH = layer->input()->getDims()[2];
        int OW = layer->input()->getDims()[3];

        ParseYOLOV2Output(blob, layer, IH, IW, IH, IW, h, w, OH, OW, 0.25, objects, anchors);  //ADD ANCHORS ALSO
    }

    // Filtering overlapping boxes //DO THIS ALSO
    std::sort(objects.begin(), objects.end()); 

    for (int i = 0; i < objects.size(); ++i) {
        if (objects[i].confidence == 0)
            continue;
        for (int j = i + 1; j < objects.size(); ++j)
            if (IntersectionOverUnion(objects[i], objects[j]) >= 0.3)
                objects[j].confidence = 0;
    }
    
    box_count = 0;
    // Drawing boxes
    for (auto &object : objects) {
        if (object.confidence < 0.25)
            continue;
        auto label = object.class_id;
        float confidence = object.confidence;
        if (confidence >= 0.25)
        {
            box_count ++;
        }
    }

    float **detections = (float **)malloc(box_count * sizeof(float *));
    int index = 0;
    for (auto &object : objects) {
        float confidence = object.confidence;
        if (confidence >= 0.25)
        {
            detections[index] = (float*)malloc(5*sizeof(float));
            detections[index][0] = object.xmin;
            detections[index][1] = object.ymin;
            detections[index][2] = object.xmax;
            detections[index][3] = object.ymax;
            detections[index][4] = confidence;
            index++;
        }
    }

    objects={};
    return detections;
}
#endif

#ifdef TRT
void ParseYOLOV2Output(const float *output_blob,
                       const unsigned long resized_im_h,
                       const unsigned long resized_im_w,
                       const unsigned long original_im_h,
                       const unsigned long original_im_w,
                       const unsigned int h,
                       const unsigned int w,
                       const unsigned int out_blob_h,
                       const unsigned int out_blob_w,
                       const double threshold,
                       std::vector<DetectionObject> &objects, std::vector<float> anchors)
{
    // --------------------------- Extracting layer parameters -------------------------------------
    const int num = 5;
    const int coords = 4;
    const int classes = 1;

    auto side = out_blob_w;
    auto side_square = side * side;

    // --------------------------- Parsing YOLO Region output -------------------------------------
    for (int i = 0; i < side_square; ++i) {
        int row = i / side;
        int col = i % side;
        for (int n = 0; n < num; ++n) {
            int obj_index = EntryIndex(side, coords, classes, n * side * side + i, coords);
            int box_index = EntryIndex(side, coords, classes, n * side * side + i, 0);
            double scale = output_blob[obj_index];
            if (scale < threshold)
                continue;

            double x = (col + output_blob[box_index + 0 * side_square]) / side * original_im_w;

            double y = (row + output_blob[box_index + 1 * side_square]) / side * original_im_h;
            double height = std::exp(output_blob[box_index + 3 * side_square]) * anchors[2 * n + 1] / side * original_im_h;
            double width  = std::exp(output_blob[box_index + 2 * side_square]) * anchors[2 * n] / side * original_im_w;
            for (int j = 0; j < classes; ++j) {
                int class_index = EntryIndex(side, coords, classes, n * side_square + i, coords + 1 + j);
                float prob = scale * output_blob[class_index];
                if (prob < threshold)
                    continue;


                DetectionObject obj(x, y, height, width, j, prob,
                                    static_cast<float>(h) / static_cast<float>(resized_im_h),
                                    static_cast<float>(w) / static_cast<float>(resized_im_w),
                                    h, w);
                objects.push_back(obj);
            }
        }
    }
}

float** postProcessLipDetect(TensorRTInferenceManager *LipDetectNet, float* output_buffer, int c, int h, int w, int &box_count) {

    std::vector<DetectionObject> objects;
    std::vector<float> anchors =  {1.08,1.19,  3.42,4.41,  6.63,11.38,  9.42,5.11,  16.62,10.52}; 
    int IC, IH, IW;

    IC = LipDetectNet->iChannels; IH = LipDetectNet->iHeight; IW = LipDetectNet->iWidth;

	for (int i = 0; i < LipDetectNet->oDims.size(); ++i){
      int OH = LipDetectNet->oDims[i][1];
      int OW = LipDetectNet->oDims[i][2];

      ParseYOLOV2Output(output_buffer, IH, IW, IH, IW, h, w, OH, OW, 0.25, objects, anchors);  //ADD ANCHORS ALSO
    }

    // Filtering overlapping boxes //DO THIS ALSO
    std::sort(objects.begin(), objects.end()); 

    for (int i = 0; i < objects.size(); ++i) {
        if (objects[i].confidence == 0)
            continue;
        for (int j = i + 1; j < objects.size(); ++j)
            if (IntersectionOverUnion(objects[i], objects[j]) >= 0.3)
                objects[j].confidence = 0;
    }
    
    box_count = 0;
    // Drawing boxes
    for (auto &object : objects) {
        if (object.confidence < 0.25)
            continue;
        auto label = object.class_id;
        float confidence = object.confidence;
        if (confidence >= 0.25)
        {
            box_count ++;
        }
    }

    float **detections = (float **)malloc(box_count * sizeof(float *));
    int index = 0;
    for (auto &object : objects) {
        float confidence = object.confidence;
        if (confidence >= 0.25)
        {
            detections[index] = (float*)malloc(5*sizeof(float));
            detections[index][0] = object.xmin;
            detections[index][1] = object.ymin;
            detections[index][2] = object.xmax;
            detections[index][3] = object.ymax;
            detections[index][4] = confidence;
            index++;
        }
    }

    objects={};
    return detections;
}
#endif