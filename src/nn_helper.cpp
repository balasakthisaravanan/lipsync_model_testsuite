#define NOMINMAX
#include "nn_helper.hpp"
#include "math.h"

#define FACE_DETECT_BATCH_SIZE 1
void CreateNetworkInput(unsigned char *image_data,
                        float *f_image, int channels, int height,
                        int width,
                        bool normalize)
{
  int max_value = normalize?255.:1.;
  int image_index, f_image_index;
  int num = 1;
  for (int n = 0; n < num; n++)
    for (int c = 0; c < channels; c++)
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {

                image_index = n * height * width * channels +
                                    h * width * channels + w * channels +
                                    (channels - c - 1);
                f_image_index = n * channels * height * width +
                                    c * height * width + h * width + w;

                float n_value =
                    (((unsigned char *)image_data)[image_index]) / (float)max_value;
                // f_image[f_image_index] = (n_value - net->mean_value[c]) / net->std_value[c];
                f_image[f_image_index] = n_value;
            }
        }
}

void emb_image(float *input, int w1, int h1, float *output,
            int w2, int h2, int dx, int dy, int channels)
{
    int x,y,k;

    for(k = 0; k < channels; ++k){
        #if !__APPLE__
        #pragma omp parallel for private(y)
        #endif        
        for(y = 0; y < h1; ++y){
            for(x = 0; x < w1; ++x){
                output[k * h2 * w2 + (dy + y) * w2 + (dx + x)]
                        = input[ k * h1 * w1 + y * w1 + x];
            }
        }
    }
}

void DarknetResizeImage(float *input, float *output, int o_w, int o_h, int r_w,
                    int r_h, int channels)
{
    float *part = (float *)malloc(r_w * o_h * channels * sizeof(float));
    if(!part) printf("malloc Fail: Unable to allocate heap space!\n");
    // image part = make_image(w, o_h, channels);
    int r, c, k;
    float w_scale = (float)(o_w - 1) / (r_w - 1);
    float h_scale = (float)(o_h - 1) / (r_h - 1);

    int oh_ow = o_h * o_w;
    int oh_rw = o_h * r_w;
    int rh_rw = r_h * r_w;

    for (k = 0; k < channels; ++k) {
        #if !__APPLE__
        #pragma omp parallel for private(y)
        #endif            
        for (r = 0; r < o_h; ++r) {
                for (c = 0; c < r_w - 1; ++c) {
                    float sx = c * w_scale;
                    int ix = (int)sx;
                    float dx = sx - ix;
                    float val = (1 - dx) * input[k * oh_ow + r * o_w + ix] +  // get_pixel(im, ix, r, k) +
                                dx * input[k * oh_ow + r * o_w + (ix + 1)];  // get_pixel(im, ix + 1, r, k);
                    part[k * oh_rw + r * r_w + c] = val;  // set_pixel(part, c, r, k, val);
                }
                part[k * oh_rw + r * r_w + c] = input[k * oh_ow + r * o_w + o_w - 1];  // get_pixel(im, o_w - 1, r, k);
            }
    }
    for (k = 0; k < channels; ++k) {
        #if !__APPLE__
        #pragma omp parallel for private(y)
        #endif        
        for (r = 0; r < r_h; ++r) {
            float sy = r * h_scale;
            int iy = (int)sy;
            float dy = sy - iy;
            for (c = 0; c < r_w; ++c) {
                float val = (1 - dy) * part[k * oh_rw + iy * r_w + c];  // get_pixel(part, c, iy, k);
                output[k * rh_rw + r * r_w + c] = val;  // set_pixel(resized, c, r, k, val);
            }
            if (r == r_h - 1) continue;
            for (c = 0; c < r_w; ++c) {
                float val = dy * part[k * oh_rw + (iy + 1) * r_w + c];  // get_pixel(part, c, iy + 1, k);
                output[k * rh_rw + r * r_w + c] += val;  // add_pixel(resized, c, r, k, val);
            }
        }
    }
    free(part);
}

void GetLetterBoxImage(float *input, float *output, int w1, int h1,
                 int w2, int h2, int channels)
{
    int i = 0;
    int new_w = w1;
    int new_h = h1;

    if (((float)w2/w1) < ((float)h2/h1)) {
        new_w = w2;
        new_h = (h1 * w2)/w1;
    } else {
        new_h = h2;
        new_w = (w1 * h2)/h1;
    }

    float *inter = (float*)malloc(new_w * new_h * channels * sizeof(float));
    if(!inter) printf("malloc Fail: Unable to allocate heap space!\n");

    DarknetResizeImage(input, inter, w1, h1, new_w, new_h, channels);
    for(i = 0; i < w2 * h2 *channels; ++i)
        output[i] = 0.5;
    emb_image(inter, new_w, new_h, output, w2, h2,
             (w2-new_w)/2, (h2-new_h)/2, channels);
    free(inter);
}

void yoloPreprocessImage(unsigned char *image_data, float *resized_image, int image_height, int image_width, int resized_height, int resized_width, int channels)
{
    float *f_image;

    f_image = (float *)malloc(channels * image_height * image_width * sizeof(float));
    if(!f_image) printf("malloc Fail: Unable to allocate heap space!\n");
    CreateNetworkInput(image_data, f_image, channels, image_height, image_width, false);

    if(!resized_image) printf("malloc Fail Unable to allocate heap space!\n");
    GetLetterBoxImage(f_image, resized_image, image_width, image_height, resized_height, resized_width, channels);

    free(f_image);
}

std::vector<Face> nonMaximumSuppression(const float nmsThresh, std::vector<Face> binfo)
{
    auto overlap1D = [](float x1min, float x1max, float x2min, float x2max) -> float {
        if (x1min > x2min)
        {
            std::swap(x1min, x2min);
            std::swap(x1max, x2max);
        }
        return x1max < x2min ? 0 : std::min(x1max, x2max) - x2min;
    };
    auto computeIoU = [&overlap1D](Box& bbox1, Box& bbox2) -> float {
        float overlapX = overlap1D(bbox1.x1, bbox1.x2, bbox2.x1, bbox2.x2);
        float overlapY = overlap1D(bbox1.y1, bbox1.y2, bbox2.y1, bbox2.y2);
        float area1 = (bbox1.x2 - bbox1.x1) * (bbox1.y2 - bbox1.y1);
        float area2 = (bbox2.x2 - bbox2.x1) * (bbox2.y2 - bbox2.y1);
        float overlap2D = overlapX * overlapY;
        float u = area1 + area2 - overlap2D;
        return u == 0 ? 0 : overlap2D / u;
    };

    std::stable_sort(binfo.begin(), binfo.end(),
                     [](const Face& b1, const Face& b2) { return b1.probability > b2.probability; });
    std::vector<Face> out;
    for (auto i : binfo)
    {
        bool keep = true;
        for (auto j : out)
        {
            if (keep)
            {
                float overlap = computeIoU(i.box, j.box);
                keep = overlap <= nmsThresh;
            }
            else
                break;
        }
        if (keep) out.push_back(i);
    }
    return out;
}

// #if GPU_DEVICE
float clamp(const float val, const float minVal, const float maxVal)
{
    assert(minVal <= maxVal);
    return std::min(maxVal, std::max(minVal, val));
}

Box convertBBox(const float& bx, const float& by, const float& bw, const float& bh,
                 const int& stride)
{
    Box b;
    // Restore coordinates to network input resolution
    float x = bx * stride;
    float y = by * stride;

    b.x1 = x - bw / 2;
    b.x2 = x + bw / 2;

    b.y1 = y - bh / 2;
    b.y2 = y + bh / 2;

    return b;
}

std::vector<Face> decodeTensor(const int& imageH, const int& imageW,
                                const int m_InputW, const int m_InputH,
                                const float* detections, const std::vector<int> mask,
                                const unsigned int gridSize, const unsigned int stride)
{
    const unsigned int m_NumBBoxes = 3;
    const std::vector<float> m_Anchors = {10.0, 14.0, 23.0, 27.0, 37.0, 58.0, 81.0, 82.0, 135.0, 169.0, 344.0, 319.0};
    const unsigned int m_NumOutputClasses = 1;
    const float m_ProbThresh = 0.35;
    float scalingFactor
        = std::min(static_cast<float>(m_InputW) / imageW, static_cast<float>(m_InputH) / imageH);
    std::vector<Face> binfo;
    for (unsigned int y = 0; y < gridSize; ++y)
    {
        for (unsigned int x = 0; x < gridSize; ++x)
        {
            for (unsigned int b = 0; b < m_NumBBoxes; ++b)
            {
                const float pw = m_Anchors[mask[b] * 2];
                const float ph = m_Anchors[mask[b] * 2 + 1];

                const int numGridCells = gridSize * gridSize;
                const int bbindex = y * gridSize + x;
                const float bx
                    = x + detections[bbindex + numGridCells * (b * (5 + m_NumOutputClasses) + 0)];
                const float by
                    = y + detections[bbindex + numGridCells * (b * (5 + m_NumOutputClasses) + 1)];
                const float bw
                    = pw * detections[bbindex + numGridCells * (b * (5 + m_NumOutputClasses) + 2)];
                const float bh
                    = ph * detections[bbindex + numGridCells * (b * (5 + m_NumOutputClasses) + 3)];

                const float objectness
                    = detections[bbindex + numGridCells * (b * (5 + m_NumOutputClasses) + 4)];

                float maxProb = 0.0f;
                int maxIndex = -1;

                for (unsigned int i = 0; i < m_NumOutputClasses; ++i)
                {
                    float prob
                        = (detections[bbindex
                                      + numGridCells * (b * (5 + m_NumOutputClasses) + (5 + i))]);

                    if (prob > maxProb)
                    {
                        maxProb = prob;
                        maxIndex = i;
                    }
                }
                maxProb = objectness * maxProb;

                if (maxProb > m_ProbThresh)
                {
                    Face bbi;
                    bbi.box = convertBBox(bx, by, bw, bh, stride);

                    // Undo Letterbox
                    float x_correction = (m_InputW - scalingFactor * imageW) / 2;
                    float y_correction = (m_InputH - scalingFactor * imageH) / 2;
                    bbi.box.x1 -= x_correction;
                    bbi.box.x2 -= x_correction;
                    bbi.box.y1 -= y_correction;
                    bbi.box.y2 -= y_correction;

                    // Restore to input resolution
                    bbi.box.x1 /= scalingFactor;
                    bbi.box.x2 /= scalingFactor;
                    bbi.box.y1 /= scalingFactor;
                    bbi.box.y2 /= scalingFactor;

                    bbi.box.x1 = clamp(bbi.box.x1, 0, imageW);
                    bbi.box.x2 = clamp(bbi.box.x2, 0, imageW);
                    bbi.box.y1 = clamp(bbi.box.y1, 0, imageH);
                    bbi.box.y2 = clamp(bbi.box.y2, 0, imageH);

                    bbi.probability = maxProb;

                    binfo.push_back(bbi);
                }
            }
        }
    }
    return binfo;
}

std::vector<Face> decodeDetections(float *detection_net_output, const int& imageIdx, const int& imageH,
                                                   const int& imageW, const int detection_width, const int detection_height)
{
    std::vector<int> m_Mask1 = {3,4,5};
    std::vector<int> m_Mask2 = {1,2,3};

    const unsigned int stride_1 = 32;
    const unsigned int stride_2 = 16;
    const unsigned int gridsize_1 = detection_height / stride_1;
    const unsigned int gridsize_2 = detection_height / stride_2;

    std::vector<Face> binfo;
    std::vector<Face> binfo1
        = decodeTensor(imageH, imageW, detection_width, detection_height, &detection_net_output[0], m_Mask1,
                       gridsize_1, stride_1);
    std::vector<Face> binfo2
        = decodeTensor(imageH, imageW, detection_width, detection_height, &detection_net_output[3042], m_Mask2,
                       gridsize_2, stride_2);
    binfo.insert(binfo.end(), binfo1.begin(), binfo1.end());
    binfo.insert(binfo.end(), binfo2.begin(), binfo2.end());
    return binfo;
}

void postprocessDetections(float *detection_net_output, int image_width, int image_height,
                            int detection_width, int detection_height, float NMSThresh)
{
    int ctr = 0;
    for (int imageIdx = 0; imageIdx < FACE_DETECT_BATCH_SIZE; ++imageIdx)
    {
        auto binfo = decodeDetections(detection_net_output, imageIdx, image_height, image_width, detection_width, detection_height);
        auto remaining = nonMaximumSuppression(NMSThresh, binfo);
        detection_net_output[ctr++] = (float)remaining.size();
        for (auto b : remaining)
        {
            detection_net_output[ctr++] = b.box.x1;
            detection_net_output[ctr++] = b.box.y1;
            detection_net_output[ctr++] = b.box.x2;
            detection_net_output[ctr++] = b.box.y2;
            detection_net_output[ctr++] = b.probability;
        }
    }
}

// #else
double IntersectionOverUnion(const Face &box_1, const Face &box_2) {
    double width_of_overlap_area  = fmin(box_1.box.x2, box_2.box.x2) - fmax(box_1.box.x1, box_2.box.x1);
    double height_of_overlap_area = fmin(box_1.box.y2, box_2.box.y2) - fmax(box_1.box.y1, box_2.box.y1);
    double area_of_overlap;

    if (width_of_overlap_area < 0 || height_of_overlap_area < 0)
        area_of_overlap = 0;
    else
        area_of_overlap = width_of_overlap_area * height_of_overlap_area;

    double box_1_area = (box_1.box.y2 - box_1.box.y1)  * (box_1.box.x2 - box_1.box.x1);
    double box_2_area = (box_2.box.y2 - box_2.box.y1)  * (box_2.box.x2 - box_2.box.x1);
    double area_of_union = (box_1_area + box_2_area - area_of_overlap + 1e-05 );
    return area_of_overlap / area_of_union;
}

void isItwithinImageBoundaries(float *xmin, float *ymin, float *xmax, float *ymax, int original_im_h, int original_im_w) {
    if(*(xmin) < 0) *(xmin) = 0;
    if(*(xmin) > original_im_w) *(xmin) = original_im_w;
    if(*(xmax) < 0) *(xmax) = 0;
    if(*(xmax) > original_im_w) *(xmax) = original_im_w;
    if(*(ymin) < 0) *(ymin) = 0;
    if(*(ymin) > original_im_h) *(ymin) = original_im_h;
    if(*(ymax) < 0) *(ymax) = 0;
    if(*(ymax) > original_im_h) *(ymax) = original_im_h;
}

static int EntryIndex(int side, int lcoords, int lclasses, int location, int entry) {
    int n = location / (side * side);
    int loc = location % (side * side);
    return n * side * side * (lcoords + lclasses + 1) + entry * side * side + loc;
}

void parseYoloOutputForBoxOutputs(float *network_out, int resized_im_h, int resized_im_w,
                                int original_im_h, int original_im_w, std::vector<long int> output_buffer_sizes, float conf_thresh, float iou_thresh) {

    #if 1
        // multi-output (2 yolo outputs) model post-processing. Meant to be used only for that model
        std::vector<Face> binfo;
        int idxOutput=0;

        int yoloOutputScales[] = {13, 26};
        
        for (int lidx=0; lidx<2; lidx++) {

            const int out_blob_h = yoloOutputScales[lidx];
            const int out_blob_w = yoloOutputScales[lidx];
            
            // --------------------------- Extracting layer parameters -------------------------------------
            auto coords = 4;
            auto classes = 1;
            
            std::vector<float> anchors = {10,14,  23,27,  37,58,  81,82,  135,169,  344,319};


            int num = 3;
            int mask[] = {3, 4, 5, 1, 2, 3};

            std::vector<float> maskedAnchors(num * 2);
            for (int i = 0; i < num; ++i) {
                maskedAnchors[i * 2] = anchors[mask[i+idxOutput*3] * 2];
                maskedAnchors[i * 2 + 1] = anchors[mask[i+idxOutput*3] * 2 + 1];
            }
            anchors = maskedAnchors;

            auto side = out_blob_h;
            auto side_square = side * side;
            const float *output_blob = lidx==0?network_out:&network_out[output_buffer_sizes[lidx-1]];

            float h_scale = static_cast<float>(original_im_h) / static_cast<float>(resized_im_h);
            float w_scale = static_cast<float>(original_im_w) / static_cast<float>(resized_im_w);


            float scalingFactor  = std::min(static_cast<float>(resized_im_w)/original_im_w, static_cast<float>(resized_im_h)/original_im_h);
            float x_correction   = (resized_im_w - scalingFactor * original_im_w) / 2;
            float y_correction   = (resized_im_h - scalingFactor * original_im_h) / 2;
            
            // --------------------------- Parsing YOLO Region output -------------------------------------
            for (int i = 0; i < side_square; ++i) {
                    int row = i / side;
                    int col = i % side;
                    for (int n = 0; n < num; ++n) {
                        
                        int obj_index = EntryIndex(side, coords, classes, n * side * side + i, coords);
                        int box_index = EntryIndex(side, coords, classes, n * side * side + i, 0);
                        float scale = output_blob[obj_index];
                        if (scale < conf_thresh)
                            continue;
                        
                        double x = (col + output_blob[box_index + 0 * side_square]) / side * resized_im_w;
                        double y = (row + output_blob[box_index + 1 * side_square]) / side * resized_im_h;
                        double height = std::exp(output_blob[box_index + 3 * side_square]) * anchors[2 * n + 1];
                        double width = std::exp(output_blob[box_index + 2 * side_square]) * anchors[2 * n];
                        
                        for (int j = 0; j < classes; ++j) {
                            int class_index = EntryIndex(side, coords, classes, n * side_square + i, coords + 1 + j);
                            float prob = scale * output_blob[class_index];
                            if (prob < conf_thresh)
                                continue;

                            float xmin   = (x - width/2 - x_correction) / scalingFactor;
                            float ymin   = (y - height/2 - y_correction) / scalingFactor;
                            float xmax   = (x + width/2 - x_correction) / scalingFactor;
                            float ymax   = (y + height/2 - y_correction) / scalingFactor;
                            isItwithinImageBoundaries(&xmin, &ymin, &xmax, &ymax, original_im_h, original_im_w);

                            Face obj(Box(xmin, ymin, xmax, ymax), prob);
                            binfo.push_back(obj);
                        }
                    }
                }
            idxOutput++;
            }
    #else
        // single output (output_boxes) model post-processing. Meant to be used only for that model

        float *output_blob = network_out;
        float scalingFactor  = std::min(static_cast<float>(resized_im_w)/original_im_w, static_cast<float>(resized_im_h)/original_im_h);
        float x_correction   = (resized_im_w - scalingFactor * original_im_w) / 2;
        float y_correction   = (resized_im_h - scalingFactor * original_im_h) / 2;

        std::vector<Face> binfo;

        for (int i = 0; i < output_buffer_sizes[0]/6; ++i) {
            int offset   = i * 6;
            float xmin   = (output_blob[offset + 0] - x_correction) / scalingFactor;
            float ymin   = (output_blob[offset + 1] - y_correction) / scalingFactor;
            float xmax   = (output_blob[offset + 2] - x_correction) / scalingFactor;
            float ymax   = (output_blob[offset + 3] - y_correction) / scalingFactor;
            float prob   = output_blob[offset + 4];
            float class_id = output_blob[offset + 5];

            if (prob<conf_thresh)
                continue;
            isItwithinImageBoundaries(&xmin, &ymin, &xmax, &ymax, original_im_h, original_im_w);
            if((int)xmin == (int)xmax || (int)ymin == (int)ymax)
                continue;

            Face obj(Box(xmin, ymin, xmax, ymax), prob);
            binfo.push_back(obj);
        }
    #endif
        auto remaining = nonMaximumSuppression(iou_thresh, binfo);
        int index = 1;
        int face_count = 0;
        for (auto b : remaining){
            *(network_out + index) = b.box.x1;
            index++;
            *(network_out + index) = b.box.y1;
            index++;
            *(network_out + index) = b.box.x2;
            index++;
            *(network_out + index) = b.box.y2;
            index++;
            *(network_out + index) = b.probability;
            index++;
            face_count++;
        }
        *(network_out+0) = face_count;
}
