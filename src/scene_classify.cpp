#include "scene_classify.hpp"

SceneClassify::SceneClassify(string scene_model, int batch_size){
#ifdef TRT
	SceneNet = new TensorRTInferenceManager(scene_model, batch_size);
#elif OPENVINO
	SceneNet = new OpenVINOInferenceManager(scene_model, batch_size);
#endif
	input_buffer  = new float[SIZE*SIZE*3];
	output_buffer = new float[SceneNet->output_buffer_sizes[0]];
	valid = false;
}

int SceneClassify::pre_process(Mat frame){
	Mat image;
	resize(frame, image, Size(SIZE, SIZE));

	int IC, IH, IW;
	IC = SceneNet->iHeight;
	IH = SceneNet->iWidth;
	IW = SceneNet->iChannels;

	for (int i = 0; i < IH; i++)
	{
		for (int j = 0; j < IW; j++)
		{
			for (int k = 0; k < IC; k++)
			{
				input_buffer[i + j * IH + k * IW * IH] = (image.data)[i * IC + j * IW * IC + k] / 255.0f;
			}
		}
	}

	return 0;
}

int SceneClassify::execute(){
#ifdef TRT
	SceneNet->runInference(input_buffer, output_buffer, 1);
#elif OPENVINO
	SceneNet->runInference(input_buffer, output_buffer);
#endif
	return 0;
}

int SceneClassify::post_process(){
	if(output_buffer[1] >= output_buffer[0]) valid = true;
	return 0;
}

pair<bool, float> SceneClassify::get_output(){
	if(valid) return {valid, output_buffer[1]};
	else return {valid, output_buffer[0]};
}