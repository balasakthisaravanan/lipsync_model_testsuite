#include "lip_mvt.hpp"
#include "lip_utils.hpp"

LipMvt::LipMvt(string lip_mvt_model, int batch_size)
{
#ifdef TRT
    LipMvtNet = new TensorRTInferenceManager(lip_mvt_model, batch_size);
#elif OPENVINO
    LipMvtNet = new OpenVINOInferenceManager(lip_mvt_model, batch_size);
#endif
    input_buffer = new unsigned char[batch_size * SIZE * SIZE * 10];
    output_buffer = new float[LipMvtNet->output_buffer_sizes[0]];
    orig_w = orig_h = orig_c = 0;
}

int LipMvt::pre_process(vector<Mat> lips, int bound)
{
    Mat gray_lip1, gray_lip2, flow, flows[2], imgX_decoded, imgY_decoded;
    vector<unsigned char> imgX_encoded;
    vector<unsigned char> imgY_encoded;
    Mat imgX(Size(SIZE,SIZE), CV_8UC1);
    Mat imgY(Size(SIZE,SIZE), CV_8UC1);
    for (int lip_itr = 0, buff_itr = 0; lip_itr < lips.size() - 1; lip_itr++, buff_itr += 2)
    {
        resize(lips[lip_itr], gray_lip1, Size(SIZE, SIZE));
        resize(lips[lip_itr + 1], gray_lip2, Size(SIZE, SIZE));
        cvtColor(gray_lip1, gray_lip1, COLOR_BGR2GRAY);
        cvtColor(gray_lip2, gray_lip2, COLOR_BGR2GRAY);
        calcOpticalFlowFarneback(gray_lip1,
                                 gray_lip2,
                                 flow, 0.702, 5, 10, 2, 7, 1.5,
                                 OPTFLOW_FARNEBACK_GAUSSIAN);
        split(flow, flows);
        convertFlowToImage(flows[0], flows[1], imgX, imgY, -bound, bound);
        imencode(".jpg", imgX, imgX_encoded);
        imencode(".jpg", imgY, imgY_encoded);
        imgX_decoded = cv::imdecode(Mat(imgX_encoded), 0);
        imgY_decoded = cv::imdecode(Mat(imgY_encoded), 0);
        memcpy(input_buffer + buff_itr * SIZE * SIZE, imgX_decoded.data, SIZE * SIZE);
        memcpy(input_buffer + (buff_itr + 1) * SIZE * SIZE, imgY_decoded.data, SIZE * SIZE);
    }
    return 0;
}

int LipMvt::execute()
{

    LipMvtNet->runInference(input_buffer, output_buffer);

    return 0;
}

int LipMvt::post_process()
{
    return 0;
}

float LipMvt::get_output()
{
    return output_buffer[1];
}