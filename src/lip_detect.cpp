#include "lip_detect.hpp"
#include "lip_utils.hpp"

LipDetect::LipDetect(string lip_model, int batch_size){
#ifdef TRT
	LipNet = new TensorRTInferenceManager(lip_model, batch_size);
#elif OPENVINO
	LipNet = new OpenVINOInferenceManager(lip_model, batch_size);
#endif
	input_buffer  = new float[SIZE*SIZE*3];
	output_buffer = new float[LipNet->output_buffer_sizes[0]];
	orig_w = orig_h = orig_c = 0;
	lip_count = 0;
}

int LipDetect::pre_process(Mat frame){
	
	Mat image;

	frame.copyTo(image);

	orig_c = image.channels(), orig_h = image.rows, orig_w = image.cols;

	int IC = LipNet->iChannels, IH = LipNet->iHeight, IW = LipNet->iWidth;

	input_buffer = preProcessLipDetect(image.data, IC, IH, IW, orig_c, orig_h, orig_w);

	return 0;
}

int LipDetect::execute(){

	LipNet->runInference(input_buffer, output_buffer);
	
	return 0;
}

int LipDetect::post_process(){

	detections =  postProcessLipDetect(LipNet, output_buffer, orig_c, orig_h, orig_w, lip_count)[0];
	
	return 0;
}

float* LipDetect::get_output(){
	return detections;
}

int LipDetect::get_lip_count(){
	return lip_count;
}