#include "face_detect.hpp"
#include "nn_helper.hpp"

FaceDetect::FaceDetect(string face_model, int batch_size){
#ifdef TRT
	FaceNet = new TensorRTInferenceManager(face_model, batch_size);
#elif OPENVINO
	FaceNet = new OpenVINOInferenceManager(face_model, batch_size);
#endif
	input_buffer  = new float[SIZE*SIZE*3];
	output_buffer = new float[FaceNet->output_buffer_sizes[0] + FaceNet->output_buffer_sizes[1]];
	orig_w = orig_h = orig_c = 0;
}

int FaceDetect::pre_process(Mat frame){
	
	Mat image;

	frame.copyTo(image);

	orig_c = image.channels(), orig_h = image.rows, orig_w = image.cols;

	yoloPreprocessImage(image.data, input_buffer, image.rows, image.cols, SIZE, SIZE, 3);

	return 0;
}

int FaceDetect::execute(){

	FaceNet->runInference(input_buffer, output_buffer);
	
	return 0;
}

int FaceDetect::post_process(){
#ifdef TRT
	postprocessDetections(output_buffer, orig_w, orig_h, SIZE, SIZE, 0.25);
#elif OPENVINO
	parseYoloOutputForBoxOutputs(output_buffer,
                                SIZE,
                                SIZE,
                                orig_h,
                                orig_w,
                                FaceNet->output_buffer_sizes,
                                0.33,
                                0.25);
#endif
	return 0;
}

float* FaceDetect::get_output(){
	return output_buffer;
}