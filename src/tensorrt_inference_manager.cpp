#include "tensorrt_inference_manager.hpp"

#define ERR_PRINT(s) std::cerr << "Error - " << s << " " << __FILE__ << " : " << __LINE__ << std::endl;
#define ERR_PRINT(s)

#define DEBUG_INFO(...)      \
	{                        \
		printf(__VA_ARGS__); \
		fflush(stdout);      \
	}
#define DEBUG_INFO(...) ;

std::string getFileName(const std::string &s)
{

	char sep = '/';

#ifdef _WIN32
	sep = '\\';
#endif

	size_t i = s.rfind(sep, s.length());
	if (i != std::string::npos)
	{
		return (s.substr(i + 1, s.length() - i));
	}

	return ("");
}

TensorRTInferenceManager::TensorRTInferenceManager(std::string engine_path, int batch_size)
{

	DEBUG_INFO("Loading Engine File from %s", engine_path.c_str());
	std::ifstream p(engine_path, std::ios::binary | std::ios::ate);
	// get pointer to associated buffer object
	std::filebuf *pbuf = p.rdbuf();
	// get file size using buffer's members
	std::size_t size = pbuf->pubseekoff(0, p.end, p.in);
	pbuf->pubseekpos(0, p.in);
	// allocate memory to contain file data
	char *buf = new char[size];
	// get file data
	pbuf->sgetn(buf, size);
	// deserialize the engine
	TRTLogger1 gLogger_local;

	initLibNvInferPlugins(&gLogger_local, "");

	nvinfer1::IRuntime *runtime = nvinfer1::createInferRuntime(gLogger_local);

	suffix = getFileName(engine_path);

	engine = runtime->deserializeCudaEngine(buf, size, nullptr);
	assert(engine);
	context = engine->createExecutionContext();

	delete (buf);
	runtime->destroy();

	netBatchSize = batch_size;
	idimensions = engine->getBindingDimensions(0);

	if (idimensions.nbDims == 3)
	{
		iChannels = idimensions.d[0];
		iHeight = idimensions.d[1];
		iWidth = idimensions.d[2];
	}
	else if (idimensions.nbDims == 4)
	{
		iChannels = idimensions.d[1];
		iHeight = idimensions.d[2];
		iWidth = idimensions.d[3];
	}
	if (suffix == "Model1.engine")
	{
		iHeight = idimensions.d[1];
		iWidth = idimensions.d[2];
		iChannels = idimensions.d[3];
	}
	// for dynamic batch :: enqueueV2
	if (idimensions.d[0] == -1)
	{
		context->setOptimizationProfile(0);
	}

	for (int i = 1; i < engine->getNbBindings(); ++i)
	{
		outputIndex.push_back(i);
		nvinfer1::Dims odimensions = engine->getBindingDimensions(i);
		long int out_size = 1;
		std::vector<int> dim;
		for (int j = 0; j < odimensions.nbDims; j++)
		{
			dim.push_back(odimensions.d[j]);
			out_size *= abs(odimensions.d[j]);
		}
		oDims.push_back(dim);
		output_buffer_sizes.push_back(out_size);
	}

	// allocate buffers
	buffer = (void **)new void *[engine->getNbBindings()];

	// input buffer
	NV_CUDA_CHECK1(cudaMalloc(&buffer[0], netBatchSize * iChannels * iHeight * iWidth * sizeof(float)));

	// output buffers
	for (int i = 0; i < output_buffer_sizes.size(); ++i)
	{
		NV_CUDA_CHECK1(cudaMalloc(&buffer[outputIndex[i]], netBatchSize * output_buffer_sizes[i] * sizeof(float)));
	}

	//create stream
	NV_CUDA_CHECK1(cudaStreamCreate(&stream));
}

void TensorRTInferenceManager::runInference(float *input_buffer, float *output_buffer, int batch_size)
{

	NV_CUDA_CHECK1(cudaMemcpyAsync(buffer[0], input_buffer,
								   batch_size * iChannels * iHeight * iWidth * sizeof(float),
								   cudaMemcpyHostToDevice, stream));

	assert(idimensions.d[0] == -1);

	context->setBindingDimensions(0, nvinfer1::Dims4(batch_size, iHeight, iWidth, iChannels));
	context->enqueueV2(buffer, stream, nullptr);

	int starting_output_index = 0;
	for (int i = 0; i < output_buffer_sizes.size(); ++i)
	{
		NV_CUDA_CHECK1(cudaMemcpyAsync(&output_buffer[starting_output_index],
									   buffer[outputIndex[i]], batch_size * output_buffer_sizes[i] * sizeof(float),
									   cudaMemcpyDeviceToHost, stream));
		starting_output_index += (batch_size * output_buffer_sizes[i]);
	}

	cudaStreamSynchronize(stream);
}

void TensorRTInferenceManager::runInference(float *input_buffer, float *output_buffer)
{

	int debug = 0;

	NV_CUDA_CHECK1(cudaMemcpyAsync(buffer[0], input_buffer,
								   netBatchSize * iChannels * iHeight * iWidth * sizeof(float),
								   cudaMemcpyHostToDevice, stream));

	context->enqueue(netBatchSize, buffer, stream, nullptr);

	int starting_output_index = 0;

	if (debug){
		for (int i = 0; i < output_buffer_sizes.size(); ++i)
		{
			float *debug_buffer = (float *) malloc(netBatchSize * output_buffer_sizes[i] * sizeof(float));
			NV_CUDA_CHECK1(cudaMemcpyAsync(debug_buffer,
										buffer[outputIndex[i]], netBatchSize * output_buffer_sizes[i] * sizeof(float),
										cudaMemcpyDeviceToHost, stream));

			std::cout<<"copy complete"<<std::endl;
			char fname[10];
			sprintf(fname, "trt_dumps/output_%d.bin", i);
			FILE *fptr = fopen(fname, "wb");
			fwrite((void*)debug_buffer, sizeof(float), output_buffer_sizes[i], fptr);
			fclose(fptr);
			starting_output_index += (netBatchSize * output_buffer_sizes[i]);
			free(debug_buffer);
		}
	}else{
		for (int i = 0; i < output_buffer_sizes.size(); ++i)
		{
			NV_CUDA_CHECK1(cudaMemcpyAsync(&output_buffer[starting_output_index],
										buffer[outputIndex[i]], netBatchSize * output_buffer_sizes[i] * sizeof(float),
										cudaMemcpyDeviceToHost, stream));

			// for(int i=0;i<netBatchSize * output_buffer_sizes[i];i++){
			// 	std::cout<<output_buffer[i]<<
			// }
			starting_output_index += (netBatchSize * output_buffer_sizes[i]);
		}
	}

	cudaStreamSynchronize(stream);
}

void TensorRTInferenceManager::runInference(unsigned char *input_buffer, float *output_buffer)
{

	float *blob_data = (float *)malloc(netBatchSize * iChannels * iHeight * iWidth * sizeof(float));

	// copy input blob and infer
	for (size_t pid = 0; pid < netBatchSize * iChannels * iHeight * iWidth; ++pid)
	{
		blob_data[pid] = (float)input_buffer[pid];
	}

	NV_CUDA_CHECK1(cudaMemcpyAsync(buffer[0], blob_data,
								   netBatchSize * iChannels * iHeight * iWidth * sizeof(float),
								   cudaMemcpyHostToDevice, stream));

	context->enqueue(netBatchSize, buffer, stream, nullptr);

	int starting_output_index = 0;
	for (int i = 0; i < output_buffer_sizes.size(); ++i)
	{
		NV_CUDA_CHECK1(cudaMemcpyAsync(&output_buffer[starting_output_index],
									   buffer[outputIndex[i]], netBatchSize * output_buffer_sizes[i] * sizeof(float),
									   cudaMemcpyDeviceToHost, stream));
		starting_output_index += (netBatchSize * output_buffer_sizes[i]);
	}

	cudaStreamSynchronize(stream);
	free(blob_data);
}

TensorRTInferenceManager::~TensorRTInferenceManager()
{
	context->destroy();
	engine->destroy();

	for (int i = 0; i < engine->getNbBindings(); ++i)
	{
		NV_CUDA_CHECK1(cudaFree(buffer[i]));
	}
	cudaStreamDestroy(stream);
}