OBJS     =  obj/nn_helper.o obj/lip_utils.o obj/scene_classify.o obj/face_detect.o obj/lip_detect.o obj/lip_mvt.o
CFLAGS   = -Wall -g
CC       = g++ -std=c++11
INCLUDES = -I/usr/local/include/opencv4/ -I./include/
LDFLAGS  = -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc -lopencv_dnn -lopencv_videoio -lopencv_optflow -lopencv_video

CFLAGS   += -DTRT
OBJS     += obj/plugin_factory.o obj/tensorrt_inference_manager.o
INCLUDES += -I/opt/nvidia/TensorRT-7.1.3.4/include/ -I/opt/nvidia/TensorRT-7.1.3.4/samples/common/ -I/usr/local/cuda/include
LDFLAGS  += -L/opt/nvidia/TensorRT-7.1.3.4/lib/ -L/opt/nvidia/TensorRT-7.1.3.4/targets/x86_64-linux-gnu/lib/ -lnvinfer -lnvinfer_plugin -lnvparsers
LDFLAGS  += -L/usr/local/cuda/lib64/ -lcuda -lcudart -lcufft -lcublas -lcudnn -lnvrtc
KERNEL   =  ./src/kernels.o

# CFLAGS += -DOPENVINO
OBJS     += obj/openvino_inference_manager.o
INCLUDES += -I/opt/intel/openvino_2020.4.287/inference_engine/include/
INCLUDES += -I/opt/intel/openvino_2020.4.287/inference_engine/external/tbb/include/
INCLUDES += -I/opt/intel/openvino_2020.4.287/deployment_tools/ngraph/include/ngraph/
INCLUDES += -I/opt/intel/openvino_2020.4.287/deployment_tools/ngraph/include/
INCLUDES += -I/opt/intel/openvino_2020.4.287/inference_engine/include/cpp
LDFLAGS  += -L/opt/intel/openvino_2020.4.287/inference_engine/lib/intel64/ -linference_engine -linference_engine_transformations -linference_engine_legacy -lMKLDNNPlugin
LDFLAGS  += -L/opt/intel/openvino_2020.4.287/inference_engine/external/tbb/lib/ -ltbb -L/opt/intel/openvino_2020.4.287/deployment_tools/ngraph/lib/ -lngraph


all: obj $(OBJS) pipeline

obj:
	mkdir -p obj

obj/%.o: src/%.cpp include/%.hpp
	$(CC) $(INCLUDES) $(CFLAGS) $(LDFLAGS) -c -fPIC $< -o $@

obj/%.o: src/%.c include/%.h
	$(CC) $(INCLUDES) -c -fPIC $< -o $@

pipeline: pipeline.cpp
	$(CC) $(INCLUDES) pipeline.cpp $(OBJS) $(CFLAGS) $(LDFLAGS) $(KERNEL) -o pipeline

clean:
	-rm -rf obj pipeline

.cpp.o:
	${CC} ${CFLAGS} ${INCLUDES} ${SRC} -c $<
