# README #

This README is for running the Amazon LipSync Live OpenVINO and TensorRT model test suite.

### How to build? ###

* git clone https://balasakthisaravanan@bitbucket.org/balasakthisaravanan/lipsync_model_testsuite.git
* cd lipsync_model_testsuite
* In Makefile, uncomment # CFLAGS += -DOPENVINO to run with OpenVINO or # CFLAGS   += -DTRT to run with TensorRT
* make

### How to run? ###

* Create a config file with extension .log and add the following
    * video_path: /path/to/video
    * dump_crop: false / true
    * dump_path: /path/to/dump_dir
    * scene_dump: /path/to/scene_model_dump.txt
    * face_dump:/path/to/face_model_dump.txt
    * lip_dump: /path/to/lip_model_dump.txt
    * lip_mvt_dump: /path/to/lip_mvt_model_dump.txt
    * scene_model: /path/to/scene_model
    * face_model: /path/to/face_model
    * lip_model: /path/to/lip_model
    * lip_mvt_model: /path/to/lip_mvt_model

* ./pipeline config.log

### View Output Logs ###

* Each model output will be saved as <model_name.txt> [Eg: scene_classify.txt]
* Each line of the output file will be <frame_no, model output>